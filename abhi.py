
f = open("purchases.txt","r")           # open file, read-only
sp = open("store-price.txt", "w") # open file, write


#Code to move store n its price to other file
for line in f:  
    data = line.strip().split("    ") 
    if len(data) == 6:
        #assign each entry to a variable
        date, time, store, item, cost, payment = data 
        sp.write("{0}\t{1}\n".format(store, cost))
f.close()
sp.close()


sp = open("store-price.txt", "r") # open file, write
so = open("ordered-store-price.txt", "w") # open file, write
lines = sp.readlines()
lines.sort()
for line in lines:
	so.write(line)
sp.close()
so.close()

so = open("ordered-store-price.txt", "r") # open file, write
sum = 0
counter = 0
count = 0
max = 0 #As cost won't be negative we can initialise it to zero
currStore = ""
for line in so:
    data = line.strip().split("\t") 
    if len(data) == 2:
        #assign each entry to a variable
        store, cost = data 
             
        if currStore != store :
            lastStore = currStore
            currStore = store
            if counter != 0 :
                print "Store = {0}  --> Count = {1} Sum = {2} Max = {3} Min = {4} Average = {5}".format(lastStore,count,sum,max,min,(sum/count))
                sum = 0
                count = 0
                max = 0 #As cost won't be negative we can initialise it to zero
        counter += 1
        #Check for  max number
        if max < float(cost) : 
            max = float(cost)
        
        #Assign first cost to min 
        if count == 0:
            min = float(cost)
        elif min > float(cost) :
            min = float(cost) #Check for min value
        sum += float(cost) #Sum calculation
        count = count + 1   #Counting the records
so.close()

